﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr7
{
    public partial class Form1 : Form
    {
        CShapesManager manager;
        bool isCtrlPressed = false;
        bool isShiftPressed = false;
        public Form1()
        {
            InitializeComponent();
            manager = new CShapesManager(
                new CanvasDimensions(
                    (int)canvas.Location.X,
                    (int)canvas.Location.Y,
                    (int)canvas.Width,
                    (int)canvas.Height
                )
            );
            colorsPicker.Items.AddRange(mapEnumToArray<Colors>());
            shapesPicker.Items.AddRange(mapEnumToArray<ShapesTypes>());
            setSettings();
            manager.subscribe(updateHandler);
        }

        private void updateHandler(object sender, EventArgs e)
        {
            renderShapes(manager.getShapes());
            setSettings();
            trackIsSaved();
        }

        private void renderShapes(IStore<IShape> shapes)
        {
            Graphics g = canvas.CreateGraphics();
            g.Clear(Color.White);

            for (shapes.First(); !shapes.IsEol(); shapes.Next())
                shapes.GetCurrent().render(g);

            g.Dispose();
        }

        private void setSettings()
        {
            Settings s = manager.getSettings();
            shapesPicker.SelectedIndex = (int)(s.shapeType);
            colorsPicker.SelectedIndex = (int)(s.color);
            scaleField.Text = s.scaling.ToString();
        }

        ~Form1()
        {
            manager.unsubscribe(updateHandler);
        }

        private void colorsPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings s = manager.getSettings();
            s.color = (Colors)(colorsPicker.SelectedIndex);
            manager.setSettings(s);
        }

        private void scaleField_KeyUp(object sender, KeyEventArgs e)
        {
            processBtnsUp(sender, e);
            if (e.KeyCode != Keys.Enter) return;

            Settings s = manager.getSettings();
            s.scaling = (float)Convert.ToDouble(scaleField.Text);
            manager.setSettings(s);
        }

        private void processBtnsUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete) manager.deleteSelectedShapes();
            if (e.KeyCode == Keys.Right) manager.shift(CShapesManager.DX, 0);
            if (e.KeyCode == Keys.Left) manager.shift(-1 * CShapesManager.DX, 0);
            if (e.KeyCode == Keys.Up) manager.shift(0, -1 * CShapesManager.DY);
            if (e.KeyCode == Keys.Down) manager.shift(0, CShapesManager.DY);
            if (e.KeyCode == Keys.ControlKey || e.KeyCode == Keys.ShiftKey) checkCtrlShiftPress(e, false);
            if (e.KeyCode == Keys.G && isCtrlPressed) {
                if (isShiftPressed) manager.ungroupSelected();
                else manager.groupSelected();
            };
            if (e.KeyCode == Keys.S && isCtrlPressed) saveFileDialog1.ShowDialog();
            if (e.KeyCode == Keys.L && isCtrlPressed) openFileDialog1.ShowDialog();
        }

        private void canvas_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            manager.selectShape(me.X, me.Y, isCtrlPressed);
        }

        private void canvas_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs me = e as MouseEventArgs;
            manager.addShape(me.X, me.Y);
        }

        private object[] mapEnumToArray<T>()
        {
            List<T> values = GetValues<T>().ToList();
            object[] items = new object[values.Count];
            
            for(int i = 0; i < values.Count; i++) items[i] = values[i].ToString();

            return items;
        }

        private IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        private void shapesPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            Settings s = manager.getSettings();
            s.shapeType = (ShapesTypes)shapesPicker.SelectedIndex;
            manager.setSettings(s);
        }

        private void canvas_Paint(object sender, PaintEventArgs e)
        {
            renderShapes(manager.getShapes());
        }

        private void scaleField_KeyDown(object sender, KeyEventArgs e)
        {
            checkCtrlShiftPress(e);
        }

        private void colorsPicker_KeyDown(object sender, KeyEventArgs e)
        {
            checkCtrlShiftPress(e);
        }

        private void shapesPicker_KeyDown(object sender, KeyEventArgs e)
        {
            checkCtrlShiftPress(e);
        }

        private void checkCtrlShiftPress(KeyEventArgs e, bool isPressed = true)
        {
            if (e.KeyCode == Keys.ControlKey) isCtrlPressed = isPressed;
            if (e.KeyCode == Keys.ShiftKey) isShiftPressed = isPressed;
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            if (saveFileDialog1.FileName == "") return;
            manager.save(saveFileDialog1.FileName);
            trackIsSaved();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            if (openFileDialog1.FileName == "") return;
            manager.load(openFileDialog1.FileName);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (manager.IsSaved) return;
            saveFileDialog1.ShowDialog();
        }

        private void trackIsSaved()
        {
            if (manager.IsSaved) Text = "Redactor";
            else Text = "Redactor*";
        }
    }
}
