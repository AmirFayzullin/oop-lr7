﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    abstract class CShape : IShape, ISerializable<IShape>
    {
        protected int x, y;
        protected float scaling = 1;
        protected Colors color;
        protected bool selected = false;
        protected CanvasDimensions dimensions;

        public int X { get { return x; } }
        public int Y { get { return y; } }

        protected CShape() { }
        protected CShape(int _x, int _y, Colors c, CanvasDimensions d)
        {
            x = _x;
            y = _y;
            color = c;
            dimensions = d;
        }

        virtual public bool isSelected() { return selected; }

        virtual public void select(bool toSelect) { selected = toSelect; }

        virtual public bool setColor(Colors newColor) { color = newColor; return true; }
        virtual public Colors getColor() { return color; }
        virtual public float getScale() { return scaling; }

        virtual public bool setScale(float k) { scaling = k; return true; }

        virtual public void shift(int dx, int dy) {
            if (isOutOfBox(x + dx, y + dy)) return;
            x += dx; y += dy; 
        }

        abstract public bool hittest(int x, int y);

        abstract public bool isOutOfBox(int x, int y);

        abstract public void render(Graphics g);

        virtual public List<IShape> ungroup()
        {
            return new List<IShape>();
        }

        protected Color mapToColor()
        {
            Color c;
            switch (color)
            {
                case Colors.Red:
                    c = Color.Red;
                    break;
                case Colors.Green:
                    c = Color.Green;
                    break;
                case Colors.Blue:
                    c = Color.Blue;
                    break;
                default:
                    c = Color.Red;
                    break;
            }

            return c;
        }

        public void addShape(IShape s)
        {

        }

        abstract public ShapeBounds getBounds();

        abstract public void load(StreamReader s, IFactory<IShape> f);

        abstract public void save(StreamWriter s);
    }

    class CShapesFactory : IFactory<IShape>
    {
        public IShape create(char code)
        {
            IShape s = null;
            switch (code)
            {
                case 'C':
                    s = new CCircle();
                    break;
                case 'S':
                    s = new CSquare();
                    break;
                case 'T':
                    s = new CTriangle();
                    break;
                case '<':
                    s = new CShapesGroup(new List<IShape>());
                    break;
                default:
                    s = null;
                    break;
            }
            return s;
        }
    }
}
