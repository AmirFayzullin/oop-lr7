﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    class CCircle : CShape, IShape
    {
        const int initialR = 20;
        int r = initialR;

        public CCircle() { }
        public CCircle(int _x, int _y, Colors c, CanvasDimensions d) : base(_x, _y, c, d)
        {
        }

        override public bool setScale(float k) {
            float prev = scaling;
            base.setScale(k);
            r = Convert.ToInt32(initialR * k);

            if (isOutOfBox(x, y))
            {
                setScale(prev);
                return false;
            }
            return true;
        }        

        override public bool hittest(int _x, int _y)
        {
            return Math.Sqrt(Math.Pow(Math.Abs(x - _x), 2) + Math.Pow(Math.Abs(y - _y), 2)) <= r;
        }

        public override bool isOutOfBox(int x, int y)
        {
            int r = (int)(initialR * scaling);

            return x + r > dimensions.x1 ||
                    x - r < dimensions.x0 ||
                    y + r > dimensions.y1 ||
                    y - r < dimensions.y0;
        }

        override public void render(Graphics g)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(mapToColor());

            g.FillEllipse(b, new Rectangle(x - r, y - r, r * 2, r * 2));

            if (selected)
            {
                Pen p = new Pen(Color.Black);
                g.DrawRectangle(p, new Rectangle(x - r - 10, y - r - 10, r * 2 + 20, r * 2 + 20));
                p.Dispose();
            }

            b.Dispose();
        }

        public override void load(StreamReader s, IFactory<IShape> f)
        {
            string data = s.ReadLine();
            string[] parameters = data.Split((" ").ToCharArray());
            List<int> iparams = new List<int>();

            for (int i = 0; i < parameters.Length; i++)
                iparams.Add((int)Convert.ToInt32(parameters[i]));

            x = iparams[0];
            y = iparams[1];
            color = (Colors)iparams[2];
            r = iparams[3];
            scaling = r / (float)initialR;
            dimensions.x0 = iparams[4];
            dimensions.y0 = iparams[5];
            dimensions.x1 = iparams[6];
            dimensions.y1 = iparams[7];
        }

        public override void save(StreamWriter s)
        {
            s.WriteLine("C");
            s.WriteLine($"{x} {y} {(int)color} {r} {dimensions.x0} {dimensions.y0} {dimensions.x1} {dimensions.y1}");
        }

        public override ShapeBounds getBounds()
        {
            return new ShapeBounds(X - r, X + r, Y - r, Y + r);
        }
    }
}
