﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace lr7   
{
    interface ISerializable<T>
    {
        void load(StreamReader s, IFactory<T> f);
        void save(StreamWriter S);
    }
}
