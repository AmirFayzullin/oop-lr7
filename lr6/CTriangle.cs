﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7   
{
    class CTriangle : CShape, IShape
    {
        const int initialHalfWidth = 20;
        int halfWidth = initialHalfWidth;

        public CTriangle() { } 
        public CTriangle(int x, int y, Colors c, CanvasDimensions d) : base(x, y, c, d) { }

        public override bool setScale(float k)
        {
            float prev = scaling;
            base.setScale(k);
            halfWidth = Convert.ToInt32(initialHalfWidth * k);

            if (isOutOfBox(x, y))
            {
                setScale(prev);
                return false;
            }
            return true;
        }

        public override void render(Graphics g)
        {
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            SolidBrush b = new SolidBrush(mapToColor());
            Point[] points = { 
                new Point(X, Y - halfWidth), 
                new Point(X - halfWidth, Y + halfWidth), 
                new Point(X + halfWidth, Y + halfWidth) 
            };

            g.FillPolygon(b, points, System.Drawing.Drawing2D.FillMode.Winding);

            if (selected)
            {
                Pen p = new Pen(Color.Black);
                g.DrawRectangle(p, new Rectangle(x - (int)halfWidth - 10, y - (int)halfWidth - 10, (int)halfWidth * 2 + 20, (int)halfWidth * 2 + 20));
                p.Dispose();
            }

            b.Dispose();
        }

        public override bool isOutOfBox(int x, int y)
        {
            int hw = (int)(initialHalfWidth * scaling);
            return x + hw > dimensions.x1 ||
                    x - hw < dimensions.x0 ||
                    y + hw > dimensions.y1 ||
                    y - hw < dimensions.y0;
        }

        public override bool hittest(int _x, int _y)
        {
            int hw = halfWidth;

            return _x <= x + hw &&
                    _x >= x - hw &&
                    _y >= y - hw &&
                    _y <= y + hw;
        }

        public override void load(StreamReader s, IFactory<IShape> f)
        {
            string data = s.ReadLine();
            string[] parameters = data.Split((" ").ToCharArray());
            List<int> iparams = new List<int>();

            for (int i = 0; i < parameters.Length; i++)
                iparams.Add((int)Convert.ToInt32(parameters[i]));

            x = iparams[0];
            y = iparams[1];
            color = (Colors)iparams[2];
            halfWidth = iparams[3];
            scaling = halfWidth / (float)initialHalfWidth;
            dimensions.x0 = iparams[4];
            dimensions.y0 = iparams[5];
            dimensions.x1 = iparams[6];
            dimensions.y1 = iparams[7];
        }

        public override void save(StreamWriter s)
        {
            s.WriteLine("T");
            s.WriteLine($"{x} {y} {(int)color} {halfWidth} {dimensions.x0} {dimensions.y0} {dimensions.x1} {dimensions.y1}");
        }

        public override ShapeBounds getBounds()
        {
            return new ShapeBounds(X - halfWidth, X + halfWidth, Y - halfWidth, Y + halfWidth);
        }
    }
}
