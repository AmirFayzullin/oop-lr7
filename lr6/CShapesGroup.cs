﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    class CShapesGroup : IShape
    {
        delegate void applyCb(IShape s);
        List<IShape> shapes = new List<IShape>();
        public int X { get { return -1; } }
        public int Y { get { return -1; } }
        bool selected = false;

        public CShapesGroup(List<IShape> _shapes)
        {
            shapes = new List<IShape>(_shapes);
            apply((IShape s) => selected |= s.isSelected());
            apply((IShape s) => s.select(false));
        }

        public void addShape(IShape shape)
        {
            shapes.Add(shape);
        }

        public bool isSelected()
        {
            return selected;
        }

        public void select(bool toSelect)
        {
            selected = toSelect;
        }

        public Colors getColor()
        {
            Colors c = Colors.Blue;
            apply((IShape s) => c = s.getColor());
            return c;
        }

        public float getScale()
        {
            float scaling = 1;
            apply((IShape s) => scaling = s.getScale());
            return scaling;
        }

        public bool setColor(Colors c)
        {
            apply((IShape s) => s.setColor(c));
            return true;
        }

        public bool setScale(float scaling)
        {
            bool success = true;
            for (int i = 0; i < shapes.Count && success; i++)
            {
                float prev = shapes[i].getScale();
                shapes[i].setScale(scaling);
                if (shapes[i].getScale() == prev) success = false; // out of box
                shapes[i].setScale(prev);
            }

            if (!success) return false;

            for (int i = 0; i < shapes.Count; i++) shapes[i].setScale(scaling);
            return true;            
        }

        public void shift(int dx, int dy)
        {
            bool isOut = false;
            apply((IShape s) => isOut |= s.isOutOfBox(s.X + dx, s.Y + dy));
            if (isOut) return;
            apply((IShape s) => s.shift(dx, dy));
        }

        public bool isOutOfBox(int x, int y)
        {
            bool isOut = false;
            apply((IShape s) => isOut |= s.isOutOfBox(x, y));
            return isOut;
        }

        public bool hittest(int x, int y)
        {
            bool hit = false;
            apply((IShape s) => hit |= s.hittest(x, y));
            return hit;
        }

        public void render(Graphics g)
        {
            apply((IShape s) => s.render(g));

            if (!selected) return;

            ShapeBounds bounds = getBounds();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            Pen p = new Pen(Color.Black);
            g.DrawRectangle(p, new Rectangle(bounds.x0 - 10, bounds.y0 - 10, bounds.x1 - bounds.x0 + 20, bounds.y1 - bounds.y0 + 20));
            p.Dispose();
        }

        private void apply(applyCb cb)
        {
            for (int i = 0; i < shapes.Count; i++) cb(shapes[i]);
        }

        public void load(StreamReader stream, IFactory<IShape> factory)
        {
            char code = Convert.ToChar(stream.ReadLine());
            while (code != '>')
            {
                IShape s = factory.create(code);
                if (s == null) continue;
                s.load(stream, factory);
                addShape(s);
                code = Convert.ToChar(stream.ReadLine());
            }
        }

        public void save(StreamWriter stream)
        {
            stream.WriteLine("<");
            apply((IShape s) => s.save(stream));
            stream.WriteLine(">");
        }

        public List<IShape> ungroup()
        {
            apply((IShape s) => s.select(selected));
            return shapes;
        }

        public ShapeBounds getBounds()
        {
            ShapeBounds bounds = new ShapeBounds(9999, -1, 9999, -1);
            
            for(int i = 0; i < shapes.Count; i++)
            {
                ShapeBounds nestedShapeBounds = shapes[i].getBounds();
                if (nestedShapeBounds.x0 < bounds.x0) bounds.x0 = nestedShapeBounds.x0;
                if (nestedShapeBounds.x1 > bounds.x1) bounds.x1 = nestedShapeBounds.x1;
                if (nestedShapeBounds.y0 < bounds.y0) bounds.y0 = nestedShapeBounds.y0;
                if (nestedShapeBounds.y1 > bounds.y1) bounds.y1 = nestedShapeBounds.y1;
            }

            return bounds;
        }
    }
}
