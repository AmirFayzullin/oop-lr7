﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lr7
{
    struct Settings
    {
        public ShapesTypes shapeType;
        public Colors color;
        public float scaling;
        public Settings(ShapesTypes st, Colors c, float s)
        {
            color = c;
            scaling = s;
            shapeType = st;
        }

        private Settings(Settings copyFrom)
        {
            color = copyFrom.color;
            scaling = copyFrom.scaling;
            shapeType = copyFrom.shapeType;
        }

        public Settings copy() { return new Settings(this); }
    }

    struct CanvasDimensions
    {
        public int x0;
        public int y0;
        public int x1;
        public int y1;
        
        public CanvasDimensions(int _x0, int _y0, int _x1, int _y1)
        {
            x0 = _x0;
            y0 = _y0;
            x1 = _x1;
            y1 = _y1;
        }

        public CanvasDimensions(CanvasDimensions c)
        {
            x0 = c.x0;
            y0 = c.x0;
            x1 = c.x1;
            y1 = c.y1;
        }
    }
    class CShapesManager : IShapesManager
    {
        delegate void storeWalkerCb(IShape shape);
        public const int DX = 10;
        public const int DY = 10;
        readonly CanvasDimensions dimensions;

        IStore<IShape> shapes = new CShapesStore(); 
        IFactory<IShape> shapesFactory = new CShapesFactory();
        Settings settings;

        bool saved = true;  
        public bool IsSaved 
        {
            private set {
                saved = value;
                if (shapes.Count == 0) saved = true;
            }
            get { return saved; }
        }
        EventHandler observers;


        public CShapesManager(CanvasDimensions d) {
            settings = new Settings(ShapesTypes.Circle, Colors.Red, 1);
            dimensions = new CanvasDimensions(d);
        }

        public Settings getSettings() { return settings.copy(); }

        public void setSettings(Settings s) {
            if (s.scaling <= 0)
            {
                notifyObservsers();
                return;
            }
            List<IShape> selected = getSelectedShapes();

            bool success = true;
            if (settings.scaling != s.scaling) success &= setScale(s.scaling);
            if (settings.color != s.color) success &= setColor(s.color);

            if (!success)
            {
                notifyObservsers();
                return;
            }

            IsSaved = false;
            settings = s.copy();
            notifyObservsers();
        }

        private bool setScale(float scaling)
        {
            List<IShape> selected = getSelectedShapes();
            bool success = true;
            for (int i = 0; i < selected.Count; i++) success &= selected[i].setScale(scaling);

            return success;
        }

        private bool setColor(Colors c)
        {
            List<IShape> selected = getSelectedShapes();
            bool success = true;
            for (int i = 0; i < selected.Count; i++) success &= selected[i].setColor(c);
            return success;
        }

        public void shift(int dx, int dy)
        {
            List<IShape> selected = getSelectedShapes();
            for (int i = 0; i < selected.Count; i++)
            {
                selected[i].shift(dx, dy);
                IsSaved = false;
                notifyObservsers();
            }
        }

        public void addShape(int x, int y)
        {
            IShape shape = null;
            switch(settings.shapeType)
            {
                case ShapesTypes.Circle:
                    shape = new CCircle(x, y, settings.color, dimensions);
                    break;
                case ShapesTypes.Square:
                    shape = new CSquare(x, y, settings.color, dimensions);                    
                    break;
                case ShapesTypes.Triangle:
                    shape = new CTriangle(x, y, settings.color, dimensions);
                    break;
            }

            shape.setScale(settings.scaling);
            if (shape.isOutOfBox(x, y)) shape = null;

            if (shape == null) return;

            shapes.Add(shape);
            IsSaved = false;
            notifyObservsers();
        }

        public void selectShape(int x, int y, bool multiple = false)
        {
            IShape shape = shapeAt(x, y);
            if (shape == null)
            {
                removeSelections();
                notifyObservsers();
                return;
            } else if (shape != null && !multiple)
            {
                removeSelections();
            }

            shape.select(!shape.isSelected());

            settings.scaling = shape.getScale();
            settings.color = shape.getColor();
            
            notifyObservsers();
        }

        public void deleteSelectedShapes()
        {
            List<IShape> selected = getSelectedShapes();
            if (selected.Count == 0) return;

            for (int i = 0; i < selected.Count; i++)
                goViaStore((IShape s) =>
                {
                    if (s == selected[i]) shapes.RemoveCurrent();
                });
            IsSaved = false;
            notifyObservsers();
        }

        public IStore<IShape> getShapes() { return shapes; }

        public void subscribe(EventHandler handler) { observers += handler; }
        public void unsubscribe(EventHandler handler) { observers -= handler; }

        private IShape shapeAt(int x, int y)
        {
            IShape shape = null;

            goViaStore((IShape s) =>
            {
                if (s.hittest(x, y)) shape = s;
            });

            return shape;
        }

        private List<IShape> getSelectedShapes()
        {
            List<IShape> selectedShapes = new List<IShape>();

            goViaStore((IShape shape) =>
            {
                if (shape.isSelected()) selectedShapes.Add(shape);
            });

            return selectedShapes;
        }

        private void goViaStore(storeWalkerCb cb)
        {
            for (shapes.First(); !shapes.IsEol(); shapes.Next())
                cb(shapes.GetCurrent());
        }

        private void notifyObservsers()
        {
            if (observers == null) return;
            observers.Invoke(this, null);
        }

        private void removeSelections()
        {
            goViaStore((IShape s) =>
            {
                if (s.isSelected()) s.select(false);
            });
        }

        public void load(string filename)
        {
            shapes.Clear();
            shapes.Load(filename, shapesFactory);
            notifyObservsers();
        }

        public void save(string filename)
        {
            shapes.Save(filename);
            IsSaved = true;
        }



        public void ungroupSelected()
        {
            List<IShape> selected = getSelectedShapes();
            for(int i = 0; i < selected.Count; i++)
            {
                List<IShape> ungrouped = selected[i].ungroup();
                if (ungrouped.Count == 0) continue;

                goViaStore((IShape s) =>
                {
                    if (s == selected[i]) shapes.RemoveCurrent();
                });

                shapes.First();
                for (int j = 0; j < ungrouped.Count; j++) shapes.Add(ungrouped[j]);
            }

            IsSaved = false;
            notifyObservsers();
        }

        public void groupSelected()
        {
            List<IShape> selected = getSelectedShapes();
            if (selected.Count == 0) return;
            deleteSelectedShapes();
            IShape group = new CShapesGroup(selected);
            shapes.Add(group);
            IsSaved = false;
            notifyObservsers();
        }
    }
}
