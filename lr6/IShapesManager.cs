﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr7
{
    interface IShapesManager
    {
        bool IsSaved { get; }
        Settings getSettings();
        void setSettings(Settings s);
        void addShape(int x, int y);
        void selectShape(int x, int y, bool multiple);
        void deleteSelectedShapes();
        IStore<IShape> getShapes();
        void groupSelected();
        void shift(int dx, int dy);
        void subscribe(EventHandler handler);
        void unsubscribe(EventHandler handler);
        void ungroupSelected();
        void save(string filename);
        void load(string filename);
    }
}
