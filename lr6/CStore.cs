﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace lr7
{
	class Item<T>
	{
		public T value;
		public Item<T> prev;
		public Item<T> next;

		public Item(T _value, Item<T> _prev = null, Item<T> _next = null)
		{
			value = _value;
			prev = _prev;
			next = _next;
		}
	}
	abstract class CStore<T> : IStore<T>
	{
		protected Item<T> _current;
		protected Item<T> _first;
		protected Item<T> _last;
		protected int count = 0;
		public int Count
        {
			get { return count; }
        }

		public CStore()
		{
			_current = null;
			_first = null;
			_last = null;
		}

		public void Add(T value)
		{
			Item<T> item = new Item<T>(value);
			if (_first == null)
			{
				// when it's first item
				_first = item;
				_last = item;
				_current = item;
			}
			else
			{
				// pushing to the end
				_last.next = item;
				item.prev = _last;
				_last = item;
			}
			count++;
		}

		public T RemoveCurrent()
		{
			if (_current == null) return default(T);
			Item<T> prev = _current.prev;
			Item<T> next = _current.next;
			T value = _current.value;

			if (prev == null && next == null)
			{
				// if deleted element is only one in list
				_first = null;
				_last = null;
				_current = null;
			}
			else if (next == null)
			{
				// if it's last
				_last = prev;
				_last.next = null;
				_current = prev;
			}
			else if (prev == null)
			{
				// if it's first
				_first = next;
				_first.prev = null;
				_current = next;
			}
			else
			{
				// if it's in the middle
				prev.next = next;
				next.prev = prev;
				_current = next;
			}
			count--;
			return value;
		}

		public void First()
		{
			_current = _first;
		}

		public void Last()
		{
			_current = _last;
		}

		public void Next()
		{
			if (!IsEol()) _current = _current.next;
		}

		public void Prev()
		{
			if (!IsEol()) _current = _current.prev;
		}

		public bool IsEol()
		{
			return _current == null;
		}

		public T GetCurrent()
		{
			return _current.value;
		}

		public bool Contains(cmpFn<T> fn)
		{
			First();
			while (!IsEol())
			{
				if (!fn(_current.value)) { Next(); continue; }
				return true;
			}
			First();
			return false;
		}

		public void Clear()
        {
			for (First(); !IsEol(); Next()) RemoveCurrent();
		}

		abstract public void Load(string filename, IFactory<T> factory);

		abstract public void Save(string filename);

		~CStore()
		{
			Clear();
		}
	}

	class CShapesStore : CStore<IShape>
    {
		public override void Save(string filename)
		{
			StreamWriter s = new StreamWriter(filename);
			s.WriteLine(count);
			for (First(); !IsEol(); Next()) _current.value.save(s);
			s.Close();
		}

		public override void Load(string filename, IFactory<IShape> factory)
		{
			StreamReader s = new StreamReader(filename);
			int count = Convert.ToInt32(s.ReadLine());

			for (int i = 0; i < count; i++)
			{
				IShape shape;
				char code = Convert.ToChar(s.ReadLine()); 

				shape = factory.create(code);
				shape.load(s, factory);

				Add(shape);
			}
			s.Close();
		}
	}
}
