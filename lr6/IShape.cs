﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace lr7
{
    enum Colors { Red, Green, Blue }
    enum ShapesTypes { Circle, Square, Triangle }
    struct ShapeBounds
    {
        public int x0;
        public int x1;
        public int y0;
        public int y1;
        public ShapeBounds(int _x0, int _x1, int _y0, int _y1) {
            x0 = _x0; x1 = _x1; y0 = _y0; y1 = _y1; 
        }
    };
    interface IShape : ISerializable<IShape>
    {
        bool isSelected();
        void select(bool toSelect);
        int X { get; }
        int Y { get; }
        Colors getColor();
        float getScale();
        bool setColor(Colors color);

        bool isOutOfBox(int x, int y);

        bool setScale(float k);
        void shift(int dx, int dy);

        bool hittest(int _x, int _y);
        void render(Graphics g);

        ShapeBounds getBounds();
        void addShape(IShape s);
        List<IShape> ungroup();
    }
}
