﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lr7   
{
    interface IFactory<T>
    {
        T create(char code);
    }
}
